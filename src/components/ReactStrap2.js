import 'bootstrap/dist/css/bootstrap.min.css'
import { Container, Row, Col, Input, FormGroup, Label, Button, Form } from 'reactstrap';

function ReactForm2() {
    return (
        <Container>
            <Row className="mt-3">
                <Col className="text-center"><b>REGISTRATION FORM</b></Col>
            </Row>
            <Row className="mt-3">
                <Col  sm={6}>
                    <Form className="mt-3">
                        <Col>
                            <FormGroup row>
                                <Label sm={4}>
                                    First Name (*)
                                </Label>
                                <Col sm={8}>
                                    <Input placeholder="Name" type="text" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={4}>
                                    Last Name (*)
                                </Label>
                                <Col sm={8}>
                                    <Input placeholder="Last Name" type="text" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={4}>
                                    Birthday (*)
                                </Label>
                                <Col sm={8}>
                                    <Input placeholder="Birthday" type="text" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={4}>
                                    Gender (*)
                                </Label>
                                <Col sm={8}>
                                    <FormGroup
                                        check
                                        inline
                                    >
                                        <Input type="radio" />
                                        <Label >
                                            Male
                                        </Label>
                                    </FormGroup>
                                    <FormGroup
                                        check
                                        inline
                                    >
                                        <Input type="radio" />
                                        <Label >
                                            Famale
                                        </Label>
                                    </FormGroup>
                                </Col>
                            </FormGroup>
                        </Col>
                    </Form>
                </Col>
                <Col sm={6}>
                    <Form className="mt-3">
                        <Col>
                            <FormGroup row>
                                <Label sm={4}>
                                    Passport(*)
                                </Label>
                                <Col sm={8}>
                                    <Input placeholder="Passport" type="text" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={4}>
                                    Email
                                </Label>
                                <Col sm={8}>
                                    <Input placeholder="Email" type="text" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={4}>
                                    Country (*)
                                </Label>
                                <Col sm={8}>
                                    <Input placeholder="Country" type="text" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={4}>
                                    Region (*)
                                </Label>
                                <Col sm={8}>
                                    <Input placeholder="Region" type="text" />
                                </Col>
                            </FormGroup>
                        </Col>
                    </Form>
                </Col>
                <FormGroup row>
                    <Label sm={2}>
                        Subject
                    </Label>
                    <Col sm={10}>
                        <Input id="exampleText" name="text" type="textarea" />
                    </Col>
                </FormGroup>
            </Row>
            <Row className="mt-3">
                <Col className="col-12 text-center">
                    <Button outline color="success">Check Data</Button>
                    {' '}
                    <Button outline color="success">Send</Button>
                </Col>
            </Row>
        </Container>
    )
}

export default ReactForm2;