import 'bootstrap/dist/css/bootstrap.min.css';
import Image from '../asset/images/image1.jpg'
import { Container, Row, Col, Input, FormGroup, Label, Button, Form } from 'reactstrap';

function ReactForm() {
    return (
        <Container>
            <Row className="mt-3">
                <Col className="text-center"><b>HỒ SƠ NHÂN VIÊN</b></Col>
            </Row>
            <Row className="mt-3">
                <Col sm={8} >
                    <Form className="mt-3">
                        <Col>
                            <FormGroup row>
                                <Label sm={3}>
                                    Họ và tên
                                </Label>
                                <Col sm={9}>
                                    <Input placeholder="Name" type="text" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={3}>
                                    Ngày Sinh
                                </Label>
                                <Col sm={9}>
                                    <Input placeholder="Birthday" type="text" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={3}>
                                    Số điện thoại
                                </Label>
                                <Col sm={9}>
                                    <Input placeholder="Phone" type="text" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={3}>
                                    Giới tính
                                </Label>
                                <Col sm={9}>
                                    <Input placeholder="Gender" type="text" />
                                </Col>
                            </FormGroup>
                        </Col>
                    </Form>
                </Col>
                <Col sm={3}>
                    <Form className="text-center">
                        <img src={Image} alt="Info" className="img-thumbnail" style={{marginLeft:"50px"}}></img>
                    </Form>
                </Col>
            </Row>
            <Row className="mt-3">
            <FormGroup row>
                <Label sm={2}>
                    Công việc
                </Label>
                <Col sm={10}  mt={2}>
                    <Input id="exampleText" name="text"type="textarea"/>
                </Col>
            </FormGroup>
            </Row>
            <Row className="mt-3">
                <Col className="col-12 text-center">
                    <Button outline color="primary">Chi tiết</Button>
                    {' '}
                    <Button outline color="success">Kiểm Tra</Button>
                </Col>
            </Row>
        </Container>
    )
}

export default ReactForm;
